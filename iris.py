import os
import tensorflow as tf
import matplotlib.pyplot as plt

from plot_graphs import plot_graphs

BATCH_SIZE = 32


def pack_features_vector(ds_features, ds_labels):
    """Pack feature dictionary into a single array"""
    ds_features = tf.stack(list(ds_features.values()), axis=1)
    return ds_features, ds_labels


# URL of where the dataset is stored
train_dataset_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_training.csv"

# download and store local copy of dataset file -> it will be downloaded only once and then reused
train_dataset_fp = tf.keras.utils.get_file(fname=os.path.basename(train_dataset_url),
                                           origin=train_dataset_url)

print("Local copy of the dataset file is stored: {}\n".format(train_dataset_fp))

# first line of the csv file consists of following information:
# nr_samples, nr_features, class 0 name, class 1 name, class 2 name

# next lines consists of following information:
# sepal_length, sepal_width, petal_length, petal_width, label
print("Printing first 5 lines of the dataset ...")
with open(train_dataset_fp, "r") as ds_file:
    for i in range(5):
        print(ds_file.readline(), end="")

# define our file look
column_names = ['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'species']
feature_names = column_names[:-1]
label_name = column_names[-1]

print("\nFeatures: {}".format(feature_names))
print("Label: {}".format(label_name))

# define index to class name translation
class_names = ['Iris setosa', 'Iris versicolor', 'Iris virginica']

# create DS
train_ds = tf.data.experimental.make_csv_dataset(
    train_dataset_fp,
    BATCH_SIZE,
    column_names=column_names,
    label_name=label_name,
    num_epochs=1
).repeat()

# print one batch
features, labels = next(iter(train_ds))
print("\nPrinting features ...")
print(features)
print("\nPrinting labels ...")
print(labels)

# convert feature dictionary into vector
train_ds = train_ds.map(pack_features_vector)

# print one batch again
features, labels = next(iter(train_ds))
print("\nPrinting stacked features ...")
print(features[:5])
print("\nPrinting stacked labels ...")
print(labels[:5])

# define the model
inputs = tf.keras.layers.Input(shape=(4,), name="Input")
x = tf.keras.layers.BatchNormalization()(inputs)
x = tf.keras.layers.Dense(10, activation="relu", name="Dense-1")(x)
x = tf.keras.layers.Dense(10, activation="relu", name="Dense-2")(x)
outputs = tf.keras.layers.Dense(3, activation="softmax", name="Output")(x)

model = tf.keras.Model(inputs=inputs, outputs=outputs, name="iris_model")

# print model summary and information
print("\nMODEL SUMMARY:")
print(model.summary())

# get test dataset
test_url = "https://storage.googleapis.com/download.tensorflow.org/data/iris_test.csv"
test_fp = tf.keras.utils.get_file(fname=os.path.basename(test_url), origin=test_url)

test_ds = tf.data.experimental.make_csv_dataset(
    test_fp,
    BATCH_SIZE,
    column_names=column_names,
    label_name=label_name,
    num_epochs=1,
    shuffle=False).repeat()

test_ds = test_ds.map(pack_features_vector)

# compile and train the model
model.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=["accuracy"])

history = model.fit(train_ds, steps_per_epoch=4, validation_steps=1, epochs=200, validation_data=test_ds)

# plot graf of training history
plot_graphs(history, "accuracy", "IRIS")

# try prediction on one test example
test_inputs, test_labels = next(iter(test_ds))
one_example_input, one_example_label = test_inputs[:1], test_labels[:1]

print(f"\nData to be predicted: {one_example_input}\nDesired label: {one_example_label}")

# get models predictions
predictions = model(one_example_input)
print(f"Model's predictions are: {predictions}")

# get the class ID -> index of neuron with maximum activation
resulted_label = tf.argmax(predictions, axis=1)
print(f"Predicted label: {resulted_label}")
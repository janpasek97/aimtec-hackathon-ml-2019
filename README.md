# Aimtec hackathon ML 2019

## Repo files:

Training for Aimtec internal hackathon ML project 2019.

**nn_basics.pptx** - neural networks basics power point presentation

**mnist.py** - MNIST handwritten digits classification ~ 98% accuracy

**iris.py** - IRIS 3 class classification problem ~ 97% accuracy

**plot_graphs.py** - utility for plotting grap of training history using pyplot

## Other materials:

[Tensorflow 2.0 API doc](https://www.tensorflow.org/api_docs)

[Tensorflow 2.0 tutorials](https://www.tensorflow.org/tutorials/)

[Tensorflow 2.0 guids](https://www.tensorflow.org/guide)

[Neural networks - 3Blue1Brown](https://www.youtube.com/watch?v=aircAruvnKk&list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi) - 
lot of interesting videos about math, not just about NN

### For someone who is really intersted in neural nets


[Neural networks and deep learning E-Book](http://neuralnetworksanddeeplearning.com/) -
best what you can find to build up solid understanding of neural networks

[Stanford lectures on YT](https://www.youtube.com/watch?v=PySo_6S4ZAg&list=PLoROMvodv4rOABXSygHTsbvUz4G_YQhOb) - 
you can find much more there, e.g. lectures about CNN(convolutional neural networks) or NLP (natural language processing)




import matplotlib.pyplot as plt


def plot_graphs(history, string, title):
    plt.plot(history.history[string])
    plt.plot(history.history["val_" + string])
    plt.xlabel("epochs")
    plt.ylabel(string)
    plt.title(title)
    plt.legend([string, "val_" + string])
    plt.show()
    plt.clf()

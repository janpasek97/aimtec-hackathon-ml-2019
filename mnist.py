import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

from plot_graphs import plot_graphs

# get mnist dataset
mnist = tf.keras.datasets.mnist

# get data and labels split into train and test
# if the dataset is not on your local disk, it is downloaded automatically
(x_train, y_train), (x_test, y_test) = mnist.load_data()
x_train, x_test = x_train / 255.0, x_test / 255.0

x_dev, x_test, y_dev, y_test = train_test_split(x_test, y_test, test_size=0.5, random_state=42)

# display first four digits
fig, axs = plt.subplots(2, 2)
axs[0, 0].imshow(x_train[0])
axs[0, 1].imshow(x_train[1])
axs[1, 0].imshow(x_train[2])
axs[1, 1].imshow(x_train[3])
plt.show()

# print shapes of data and labels
print(f"Shape of train data: {x_train.shape}, Shape of train labels: {y_train.shape}")
print(f"Shape of dev data: {x_dev.shape}, Shape of dev labels: {y_dev.shape}")
print(f"Shape of test data: {x_test.shape}, Shape of test labels: {y_test.shape}")

# print one data example and it's label
print(f"\nTrain data example:\n{x_train[0]}")
print(f"Train label example: {y_train[0]}")

# build the model using Keras Sequential API -> model is defined as a list of layers
# data are passed from the preceding layer to next one
model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(28, 28)),
    tf.keras.layers.Dense(128, activation="relu"),
    tf.keras.layers.Dropout(0.2),
    tf.keras.layers.Dense(10, activation="softmax")
])

# compile the model -> define optimizer, loss and desired metrics
model.compile(optimizer="adam", loss="sparse_categorical_crossentropy", metrics=["accuracy"])

# train the model on train data -> 5 epochs -> all the data will be given to NN 5 times
history = model.fit(x_train, y_train, epochs=5, validation_data=(x_dev, y_dev))

# plot graf of training history
plot_graphs(history, "accuracy", "MNIST")

# evaluate model performance on testing data
model.evaluate(x_test,  y_test, verbose=2)